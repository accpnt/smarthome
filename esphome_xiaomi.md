# Building a ESPHome firmware for a Xiaomi Bedside Lamp 2

Pull latest ESPHome docker

```
sudo docker pull ghcr.io/esphome/esphome
```

Fetch the sources located at the following repository: [https://github.com/mmakaay/esphome-xiaomi_bslamp2](https://github.com/mmakaay/esphome-xiaomi_bslamp2)


Create a ```secrets.yaml``` file

```
wifi_ssid: <my_wifi_ssid>
wifi_password: <my_wifi_password>
api_encryption_key: <my_generated_encryption_key>
ota_password: <my_ota_password>
```

Check this [link](https://esphome.io/components/api.html?highlight=randomly%20generated) for generating a 32-byte base64 encoded string. 

Copy example.yaml to a custom configuration file (ie. ```bslamp2.yaml```) and build the firmware using the following command:

```
sudo docker run --rm -v "${PWD}":/config -it ghcr.io/esphome/esphome compile bslamp2.yaml
```

If everything is okay, the generated ```firmware.bin``` file should be available in the folder ```.esphome/build/bedside-lamp/.pioenvs/bedside-lamp/```

Backup the stock firmware:

```
sudo esptool -p /dev/ttyUSB0 -b 115200 read_flash 0x00000 0x400000 firmware_backup.img
```

Flash the firmware using the following commands:

```
sudo esptool.py write_flash 0x8000 partitions.bin
sudo esptool.py write_flash 0x1000 bootloader.bin
sudo esptool.py write_flash 0x10000 firmware.bin
```

Check connection on the ESPHome local portal:

```
sudo docker run --rm --net=host -v "${PWD}":/config -it ghcr.io/esphome/esphome
```
